const ApiResponse = require("./ApiResponse.model");
const { db } = require("./db.model");

class PokemonResponse{
    
    async getPokemon(){
        const apiResp = new ApiResponse();
        try{
            const result = await db.pool.query("SELECT * FROM pokemon");
            apiResp.data = result.rows || [];
        }
        catch(e){
            apiResp.status = 500;
            apiResp.error = e.message;
        }
        return apiResp;
    }

    async addPokemon(pokemon){
        const apiResp = new ApiResponse();

        if(pokemon === null){
            apiResp.status = 400;
            apiResp.error = "Please add a pokemon";
            return apiResp;
        }

        const insertQuery = {
            text:
                `INSERT INTO pokemon
                    (name, species, height, weight, abilities, type, sprite, hp, attack, defense, sp_attack, sp_defense, speed, total, active)
                VALUES
                    ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)
                RETURNING id
                `,
            values: [pokemon.name, pokemon.species, pokemon.height, pokemon.weight, pokemon.abilities, pokemon.type, pokemon.sprite, pokemon.hp, pokemon.attack, pokemon.defense, pokemon.sp_attack, pokemon.sp_defense, pokemon.speed, pokemon.total, pokemon.active]
        }

        try{
            const result = await db.pool.query(insertQuery);
            apiResp.data = {
                ...pokemon,
                id: result.rows[0].id
            } || null;
        }
        catch(e){
            console.error(e);
            apiResp.status = 500;
            apiResp.error = e.message;
        }
        return apiResp;
    }

    async updatePokemon(pokemon){
        const apiResp = new ApiResponse();

        const updateQuery = {
            text: `UPDATE pokemon
                        SET name = $1,
                            species = $2,
                            height = $3,
                            weight = $4,
                            abilities = $5,
                            type = $6,
                            sprite = $7,
                            hp = $8,
                            attack = $9,
                            defense = $10,
                            sp_attack = $11,
                            sp_defense = $12,
                            speed = $13,
                            total = $14,
                            active = $15
                    WHERE
                         id = $16
            `,
            values: [pokemon.name, pokemon.species, pokemon.height, pokemon.weight, pokemon.abilities, pokemon.type, pokemon.sprite, pokemon.hp, pokemon.attack, pokemon.defense, pokemon.sp_attack, pokemon.sp_defense, pokemon.speed, pokemon.total, pokemon.active, pokemon.id]
        }

        try{
            const result = await db.pool.query(updateQuery);
            apiResp.data = {
                ...pokemon
            } || null;
        }
        catch(e){
            console.error(e);
            apiResp.status = 500;
            apiResp.error = e.message;
        }

        return apiResp;        
    }

    deletePokemon(pokemon){
        const apiResp = new ApiResponse();
        
        const deleteQuery = {
            text: `UPDATE pokemon
                        SET active = 0
                        WHERE
                            id = $1
                    `,
            values: [pokemon.id]
        }

        try{
            const result = db.pool.query(deleteQuery);
            apiResp.data = {
                ...pokemon
            } || null;
        }
        catch(e){
            console.error(e);
            apiResp.status = 500;
            apiResp.error = e.message;
        }

        return apiResp;
    }

}

module.exports = new PokemonResponse;
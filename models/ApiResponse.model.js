class ApiResponse {
    constructor(){
        this.status = 200;
        this.data = [];
        this.success = true;
        this.error = "";
    }
}

module.exports = ApiResponse;
const express = require("express");
const router = express.Router();

const pokedex = require("../models/pokemon-response.model");

router.get("/", async (req, resp)=>{
    const apiResp = await pokedex.getPokemon();
    if(apiResp.data === null){
        apiResp.data = "Welcome to Pokemon API";
    }
    resp.status(apiResp.status).json(apiResp).end();
})

router.post("/add", async (req, resp)=>{
    const { newPokemon } = req.body || null;

    const apiResp = await pokedex.addPokemon(newPokemon);
    resp.status(apiResp.status).send(apiResp).end();
})

router.put("/update", async (req, resp)=>{
    const { newPokemon } = req.body || null;
    
    const apiResp = await pokedex.updatePokemon(newPokemon);
    resp.status(apiResp.status).send(apiResp).end();
})

router.delete("/delete", async (req, resp)=>{
    const { pokemon } = req.body || null;

    const apiResp = await pokedex.deletePokemon(pokemon);
    resp.status(apiResp.status).send(apiResp).end();
})

module.exports = router;
const express = require("express");
const port = process.env.PORT || 3000;
const app = express();
const authMiddleware = require("./middleware/auth.middleware");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(authMiddleware);


app.use((req, resp, next)=>{
    let authenticated = true;
    
    if(authenticated){
        next();
    }
    else{
        resp.json({
            error: "Not authenticated"
        });
    }
})

const authRoutes = require("./routes/auth.route");
const pokemonRoutes = require("./routes/pokemon.route");

app.use("/api/v1/", authRoutes);

app.use("/api/v1/pokemon", pokemonRoutes);

app.listen(port, ()=>{
    console.log(`Server is running on ${port}...`);
})